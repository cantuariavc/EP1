#include "filtroPretoBranco.hpp"

FiltroPretoBranco::FiltroPretoBranco()
{}

FiltroPretoBranco::~FiltroPretoBranco()
{}

void FiltroPretoBranco::aplicaFiltro(Imagem &img)
{
    Imagem* image = &img;

    for (int i = 0; i < image->getWidth(); ++i)
    {
        for (int j = 0; j < image->getHeight(); ++j)
        {
            int grayscaleValue = (0.299 * image->pixel[i][j][1]) + (0.587 * image->pixel[i][j][2]) + (0.144 * image->pixel[i][j][3]);
            
            if (grayscaleValue <= 255)
            {
                image->pixel[i][j][1] = grayscaleValue;
                image->pixel[i][j][2] = grayscaleValue;
                image->pixel[i][j][3] = grayscaleValue;
            }
            else
            {
                image->pixel[i][j][1] = image->getMaxColor();
                image->pixel[i][j][2] = image->getMaxColor();
                image->pixel[i][j][3] = image->getMaxColor();
            }
        }
    }
}

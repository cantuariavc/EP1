#include "filtroPolarizado.hpp"

FiltroPolarizado::FiltroPolarizado()
{}

FiltroPolarizado::~FiltroPolarizado()
{}

void FiltroPolarizado::aplicaFiltro(Imagem &img)
{
    Imagem* image = &img;

    for (int i = 0; i < image->getWidth(); ++i)
    {
        for (int j = 0; j < image->getHeight(); ++j)
        {
            if(image->pixel[i][j][1] < image->getMaxColor()/2)
            {
                image->pixel[i][j][1] = 0;
            }
            else
            {
                image->pixel[i][j][1] = image->getMaxColor();
            }
            if (image->pixel[i][j][2] < image->getMaxColor()/2)
            {
                image->pixel[i][j][2] = 0;
            }
            else
            {
                image->pixel[i][j][2] = image->getMaxColor();
            }
            if (image->pixel[i][j][3] < image->getMaxColor()/2)
            {
                image->pixel[i][j][3] = 0;
            }
            else
            {
                image->pixel[i][j][3] = image->getMaxColor();
            }
        }
    }
}

#include "filtroMedia.hpp"

FiltroMedia::FiltroMedia()
{}

FiltroMedia::~FiltroMedia()
{}

void FiltroMedia::aplicaFiltro(imagem &img, int tamanhoFiltro)
{
 	Image* image = &img;

    unsigned char filter[1000][1000][3];
    for (int i = 0; i <= image->getWidth(); i++)
    {
        for (int j = 0; j <= image->getHeight(); j++)
        {
            filter[i][j][1] = image->pixel[i][j][1];
            filter[i][j][2] = image->pixel[i][j][2];
            filter[i][j][3] = image->pixel[i][j][3];
        }
    }

    int limit = filterSize/2;

    for (int i = limit; i < image->getWidth(); ++i)
    {
        for(int j = limit; j < image->getHeight(); ++j)
        {
            int value[4] = {0};
            int neighbour;

            if (filterSize == 3)
            {
                neighbour = 1;
            }
            if (filterSize == 5)
            {
                neighbour = 2;
            }
            if (filterSize == 7)
            {
                neighbour = 3;
            }

            for (int x = (-1)*neighbour; x <= neighbour; x++)
            {
                for (int y = (-1)*neighbour; y <= neighbour; y++)
                {
                    value[1] += filter[i+x][j+y][1];
                    value[2] += filter[i+x][j+y][2];
                    value[3] += filter[i+x][j+y][3];
                }
            }
            value[1] /= filterSize * filterSize;
            value[2] /= filterSize * filterSize;
            value[3] /= filterSize * filterSize;

            image->pixel[i][j][1] = (unsigned char) value[1];
            image->pixel[i][j][2] = (unsigned char) value[2];
            image->pixel[i][j][3] = (unsigned char) value[3];
        }
    }
}

#include "imagem.hpp"

Imagem::Imagem()
{}

Imagem::~Imagem()
{}

void Imagem::setNumeroMagico(char numeroMagico[2])
{
    this->numeroMagico[2] = numeroMagico[2];
}

char Imagem::getNumeroMagico(void)
{
    return numeroMagico[2];
}

void  Imagem::setComentario(string comentario)
{
    this->comentario = comentario;
}

string  Imagem::getComentario(void)
{
    return comentario;
}

void Imagem::setLargura(int largura)
{
    this->largura = largura;
}

int Imagem::getLargura(void)
{
    return largura;
}

void Imagem::setAltura(int altura)
{
    this->altura = altura;
}

int Imagem::getAltura(void)
{
    return altura;
}

void Imagem::setCorMaxima(int corMaxima)
{
    this->corMaxima = corMaxima;
}

int Imagem::getCorMaxima(void)
{
    return corMaxima;
}

void Imagem::setConteudoImagem(string conteudoImagem)
{
    this->conteudoImagem = conteudoImagem;
}

string Imagem::getConteudoImagem(void)
{
    return conteudoImagem;
}

#include "servicos.hpp"
#include "filtro.hpp"
#include "imagem.hpp"

string imagemAntiga, imagemNova, filtroEscolido;
int opcao, continua;

void menu(void);
void escolha(void);
void abreImagemAntiga(void);
void escreveImagemNova(void)
void novamente(void);

int main(void)
{
    imagemAntiga = "sem imagem";
    imagemNova = "sem imagem";
    filtroEscolido = "sem filtro";
    opcao = 0;
    continua = 0;

    do
    {
        do
        {
            menu();
            escolha();
        }
        while (opcao != 0 && opcao != 4);
        novamente();
    }
    while (opcao != 0 && continua == 1);

    return(0);
}

void menu(void)
{
    do
    {
        system("clear");

        cout << endl;
        cout << "   Menu" << endl;
        cout << endl;
        cout << "   Nome da imagem antiga:  " << imagemAntiga << endl;
        cout << "   Nome da imagem nova:    " << imagemNova << endl;
        cout << "   Filtro utilizado:       " << filtroEscolido << endl;
        cout << endl;
        cout << "   1 - Adicionar nome da imagem antiga" << endl;
        cout << "   2 - Adicionar nome da imagem nova" << endl;
        cout << "   3 - Alterar filtro da imagem" << endl;
        cout << "   4 - Fazer conversão da imagem" << endl;
        cout << "   0 - Sair" << endl;
        cout << endl;
        cout << "   Opção: ";
        cin >> opcao;
        cin.ignore();
        cout << endl;
    }
    while (opcao < 0 || opcao > 4);
}

void escolha(void)
{
    Filtro *f = new Filtro();

    int tamanhoimagemAntiga, tamanhoimagemNova, filtro;

    switch(opcao)
    {
        case 1:
            cout << "   Qual é o nome da imagem antiga? ";
            cin >> imagemAntiga;
            cin.ignore();
            tamanhoimagemAntiga = imagemAntiga.size();
            if ((tamanhoimagemAntiga <= 4) || imagemAntiga[tamanhoimagemAntiga-1] != 'm' || imagemAntiga[tamanhoimagemAntiga-2] != 'p' || imagemAntiga[tamanhoimagemAntiga-3] != 'p' || imagemAntiga[tamanhoimagemAntiga-4] != '.')
            {
                imagemAntiga = imagemAntiga + ".ppm";
            }
            break;

        case 2:
            cout << "   Qual é o nome da imagem nova? ";
            cin >> imagemNova;
            cin.ignore();
            tamanhoimagemNova = imagemNova.size();
            if ((tamanhoimagemNova <= 4) || imagemNova[tamanhoimagemNova-1] != 'm' || imagemNova[tamanhoimagemNova-2] != 'p' || imagemNova[tamanhoimagemNova-3] != 'p' || imagemNova[tamanhoimagemNova-4] != '.')
            {
                imagemNova = imagemNova + ".ppm";
            }
            break;

        case 3:
            do
            {
                cout << endl;
                cout << "   Qual filtro a ser utilizado?" << endl;
                cout << "   1 - Média" << endl;
                cout << "   2 - Negativo" << endl;
                cout << "   3 - Polarizado" << endl;
                cout << "   4 - Preto e Branco" << endl;
                cout << endl;
                cout << "   Opção: ";
                cin >> filtro;
                cin.ignore();
            }
            while (opcao < 1 || opcao > 5);
            f->setFiltro(filtro);
            switch (f->getFiltro())
            {
                case 1:
                    filtroEscolido = "Média";
                    break;
                case 2:
                    filtroEscolido = "Negativo";
                    break;
                case 3:
                    filtroEscolido = "Polarizado";
                    break;
                case 4:
                    filtroEscolido = "Preto e Branco";
                    break;
            }
            break;

        case 4:
            abreImagemAntiga();
            escreveImagemNova();
            break;

        default:
            break;
    }
}

void abreImagemAntiga(void)
{
    Imagem *i = new Imagem();

    ifstream leitura;

    stringstream ss;
    string comentario, comentarioParcial, conteudoImagem;
    char numeroMagico[2];
    int largura, altura, corMaxima;

    comentario = "\0";
    altura = 0;

    imagemAntiga = "doc/" + imagemAntiga;

    leitura.open(imagemAntiga.c_str());

    if(!leitura.is_open()){
        cout << "\n\n";
        cout << "   Erro na abertura da imagem!" << endl;
        cout << "   Verifique se o nome da imagem antiga está correto!" << endl;
        cout << "\n\n";
        exit(1);
    }

    getline(leitura, numeroMagico[2]);
    imagem->setNumeroMagico(numeroMagico[2]);

    do
    {
        getline(leitura, comentarioParcial);
        ss << comentarioParcial;
        if (comentarioParcial[0] == '#')
        {
            comentario += comentarioParcial + "\n";
        }
        else
        {
            ss >> largura;
            ss >> altura;
            if (altura == 0)
                getline(leitura, altura);
        }
    }
    while (altura == 0);
    imagem->setComentario(comentario);
    imagem->setLargura(largura);
    imagem->setAltura(altura);

    getline(leitura, corMaxima);
    imagem->setCorMaxima(corMaxima);

    while (!leitura.eof())
    {
        getline(leitura, conteudoImagem);
    }
    imagem->setConteudoImagem(conteudoImagem);

    leitura.close();
}

void Servicos::escreveImagemNova(void)
{
    Imagem *imagem = new Imagem();

    ofstream escrita;

    if (imagemNova == "sem imagem")
    {
        imagemNova = "doc/novaImagem.ppm";
    }
    else
    {
        imagemNova = "doc/" + imagemNova;
    }

    escrita.open(imagemNova.c_str());

    if(!escrita.is_open()){
        cout << "\n\n";
        cout << "   Erro na criação da imagem nova imagem!" << endl;
    }

    escrita << imagem->getNumeroMagico() << endl;

    escrita << imagem->getComentario() << endl;

    escrita << imagem->getLargura() << " " << imagem->getAltura() << endl;

    escrita << imagem->getCorMaxima();

    escrita << imagem->getConteudoImagem() << endl;

    escrita.close();
}

void novamente(void)
{
    imagemAntiga = "sem imagem";
    imagemNova = "sem imagem";
    filtroEscolido = "sem filtro";

    cout << "\n\n\n";
    cout << "   Deseja fazer outra conversão?" << endl;
    cout << endl;
    cout << "   1 - Sim" << endl;
    cout << "   2 - Não" << endl;
    cout << endl;
    cout << "   Opção: ";
    cin >> continua;
    cin.ignore();
}

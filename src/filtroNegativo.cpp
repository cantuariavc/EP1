#include "filtroNegativo.hpp"

FiltroNegativo::FiltroNegativo()
{}

FiltroNegativo::~FiltroNegativo()
{}

void FiltroNegativo::aplicaFiltro(Imagem &img)
{
    Imagem *imagem = &img;

    for(int i = 0; i < imagem->getLargura(); i++)
    {
        for(int j = 0; j < imagem->getAltura(); j++)
        {
            imagem->pixel[i][j][1] = imagem->getCorMaxima() - imagem->pixel[i][j][1];
            imagem->pixel[i][j][2] = imagem->getCorMaxima() - imagem->pixel[i][j][2];
            imagem->pixel[i][j][3] = imagem->getCorMaxima() - imagem->pixel[i][j][3];
        }
    }
}

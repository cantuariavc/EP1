#ifndef FILTROPRETOBRANCO_HPP
#define FILTROPRETOBRANCO_HPP

#include "servicos.hpp"
#include "filtro.hpp"

class FiltroPretoBranco: public Filtro
{
    public:
        FiltroPretoBranco();
        ~FiltroPretoBranco();
        void aplicaFiltro(Imagem &img);
};

#endif

#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP

#include "servicos.hpp"
#include "filtro.hpp"

class FiltroMedia: public Filtro
{
    public:
        FiltroMedia();
        ~FiltroMedia();
        void aplicaFiltro(imagem &img, int tamanhoFiltro);
};

#endif

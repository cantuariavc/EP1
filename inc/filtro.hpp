#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "servicos.hpp"

class Filtro
{
    private:
        int filtro;

    public:
        Filtro();
        ~Filtro();
        void setFiltro(int filtro);
        int getFiltro(void);
        virtual void aplicaFiltro(Imagem &img);
};

#endif

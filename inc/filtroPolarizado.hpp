#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP

#include "servicos.hpp"
#include "filtro.hpp"

class FiltroPolarizado: public Filtro
{
    public:
        FiltroPolarizado();
        ~FiltroPolarizado();
        void aplicaFiltro(Imagem &img);
};

#endif

#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include "servicos.hpp"

class Imagem
{
    private:
        string comentario, conteudoImagem;
        char numeroMagico[2];
        int largura, altura, corMaxima;

    public:
        Imagem();
        ~Imagem();
        void setNumeroMagico(char numeroMagico[2]);
        char getNumeroMagico(void);
        void setComentario(string comentario);
        string getComentario(void);
        void setLargura(int largura);
        int getLargura(void);
        void setAltura(int altura);
        int getAltura(void);
        void setCorMaxima(int corMaxima);
        int getCorMaxima(void);
        void setConteudoImagem(string conteudoImagem);
        string getConteudoImagem(void);
};

#endif

#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "servicos.hpp"
#include "filtro.hpp"

class FiltroNegativo: public Filtro
{
    public:
        FiltroNegativo();
        ~FiltroNegativo();
        void aplicaFiltro(Imagem &img);
};

#endif

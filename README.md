# Incompleto - EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Aplicador de filtros em imagens de formato `.ppm`.

As imagens `.ppm` devem estar no diretório doc/

Para compilar, limpar e executar o projeto utilize esses seguintes comandos no diretório raiz do projeto:

Compilar:
```
$ make
```
Limpar:
```
$ make clean
```
Executar:
```
$ make run
```
